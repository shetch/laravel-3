<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

/* use App\Project; */

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /* $this->middleware('auth'); */

    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        /* return view('projects.index', [
            'projects' => auth()->user()->projects
        ]); */
        /* $meta = array(
            "keywords" => "Add Keywords here",
            "description" => "Add Description here",
          ); */
        /* $request->session()->push('teams', 'developers'); */
        /* $array = ["9"=> 1, "11"=>2];
        $product = collect([1,2,3,4]);
        $request->session()->push('cart', $meta);
        $data = $request->session()->all();
        dd($data);
        return view('home'); */

        /* $data = $request->session()->all();
        dd($data);
        $data = $request->session()->get('_token'); */
        /* dd($data); */
        /* $data = 'simon'; */
        /* return view('greetings', ['name' => 'Victoria']); */
        /* return view('home')->with('data', $data); */
       /* dd("hello"); */
       /* $data = $request->session()->all(); */

        if (session('success_message')) {
            Alert::success('Congratulations', 'Your item has been added to basket');
        }


        if ($request->has('id')) {
        $cart = array(
            "id" => $request->query('id'),
            "price" => 2.99,
            "qty" => 12,
        );
        $request->session()->push('cart', $cart);

        return redirect('/cart')->withSuccessMessage('Successfully Added');

      }
       
       $values = session('cart');
      /*  dd($values); */

       $string = '';
       if (!empty($values)) {
        foreach ($values as $key => $value) {
            $string .= $value['id'] . '-'. $value['price'] . '-'. $value['qty'] . '<br>';
        }
       } 

       /* dd($string); */
       return view('shop.cart')->with('cart', $string);

    }

    public function add(Request $request)
    {
        $cart = array(
            "id" => 1,
            "price" => 2.99,
            "qty" => 12,
          );

          $request->session()->push('cart', $cart);
          /* $data = $request->session()->all(); */
          /* dd($data); */


          $values = session('cart');
          /* dd($values); */
   
          $string = '';
          
          foreach ($values as $key => $value) {
            $string .= $value['id'] . '-'. $value['price'] . '-'. $value['qty'] . '<br>';
          }

          return view('shop.cart')->with('cart', $string);



    }
}
