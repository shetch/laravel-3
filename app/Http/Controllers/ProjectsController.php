<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Project;

class ProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('auth')->only(['index', 'show', 'create', 'store', 'edit', 'destroy', 'update']);*/
    }

    public function index(Request $request)
    {
        
      /* $projects = \App\Project::all(); */
      /* alternative if using above -> use App\Project;  */
      /*$projects = Project::all();*/

      //auth()->id() 4
      //auth()->user() User
      //auth()->check() boolean if user logged in
      //auth()->guest()

      /*$projects = Project::where('owner_id', auth()->id())->get();*/

      /* return $projects;
      return view('projects.index'); */



      /*$projects = auth()->user()->projects;
      return view('projects.index', compact('projects'));*/


    /*$project = Project::findOrFail($id);*/

    /*$project->title = request('title');
    $project->description = request('description');
    $project->save();*/


      $request->session()->forget('cart');

      $id = request()->input('id');
      $set = request()->input('set');
      
      if ($id) {
        $project = Project::findOrFail($id);
        $project->id = $id;
        $project->status = $set;
        $project->save();
      }

      


      return view('projects.index', [
          'projects' => auth()->user()->projects
      ]);


    }

    public function show(Project $project)
    {
        /*$project = Project::findOrFail($id);*/
        return view('projects.show', compact('project'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store()
    {

        /*Not good practice for security  */
        /*Project::create(request()->all());*/



        /*return $validated;*/

        /*Project::create($validated);*/
        /*Project::create($validated + ['owner_id' => auth()->id()]);*/



        /*Project::create(request(['title','description']));*/


        //return request()->all();
        //return request('title');

        /*$project = new Project();
        $project->title = request('title');
        $project->description = request('description');
        $project->save();*/

        /*dd(request(['title', 'description']));*/

        /*return [
            'title' => request('title'),
            'description' => request('description')
        ];*/

        /*Project::create([
            'title' => request('title'),
            'description' => request('description')
        ]);*/

        /*request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]);*/

        /*request()->validate([
            'title' => ['required','min:3', 'max:64'],
            'description' => ['required','min:10']
        ]);*/


        /*$validated = request()->validate([
            'title' => ['required','min:3', 'max:64'],
            'description' => ['required','min:10']
        ]);*/

        /* $validated = $this->validateProject();

        $validated['owner_id'] = auth()->id();

        $project = Project::create($validated); */

        /* Mail::to($project->owner->email)->send(
          new ProjectCreated($project)
        ); */

        $validated = $this->validateProject();
        $validated['branch'] = request('branch');
        $validated['owner_id'] = auth()->id();

        $project = Project::create($validated);

        return redirect('/projects');

    }
    
    public function post(Project $project, Request $request)
    {
        /* $project->update(request(['id','status'])); */
        

        DB::table('projects')
        ->where('id', request('id'))
        ->update(['status' => request('status')]);

        return response()->json(['success'=> request('msg')]);

    }


    public function edit(Project $project)
    {
        /*$project = Project::findOrFail($id);*/
        return view('projects.edit', compact('project'));
    }

    public function update(Project $project)
    {

        //dd("hello");
        //dd(request()->all());
        /*$project = Project::findOrFail($id);*/

        /*$project->title = request('title');
        $project->description = request('description');
        $project->save();*/



        /*$validated['owner_id'] = auth()->id();*/
        /*$project = Project::update($validated);*/

        /*$validated = request()->validate([
            'title' => ['required','min:3', 'max:64'],
            'description' => ['required','min:10']
        ]);

        $project->update(request(['title','description']));*/

        //can be used in conjunction with public function below
        $project->update($this->validateProject());

        return redirect('/projects');
    }


    public function destroy(Project $project)
    {
        /* Project::findOrFail($id)->delete();*/

        $project->delete();
        return redirect('/projects');
    }

    public function validateProject()
    {
        return $validated = request()->validate([
            'task_number' => ['required','min:7', 'max:7'],
            'branch' => [],
            'customer' => [],
            'notes' => [],
            'mode' => [],
            'note' => ['required','min:5'],
            'status' => ['required']
        ]);
    }



}
