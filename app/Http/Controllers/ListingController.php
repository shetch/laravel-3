<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Product;

class ListingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /* $this->middleware('auth'); */
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        /* $request->session()->forget('cart'); */
        /* return view('projects.index', [
            'projects' => auth()->user()->projects
        ]); */
        /* $cart = array(
            "id" => 1,
            "price" => 2.99,
            "qty" => 12,
          ); */
        /* $request->session()->push('teams', 'developers'); */
        /* $array = ["9"=> 1, "11"=>2];
        $product = collect([1,2,3,4]); */
       /*  $request->session()->push('cart', $cart); */
        /* $data = $request->session()->all(); */
        /* dd($data); */
        
        /*$products = Product::all()->limit(5);*/
        $products = DB::table('products')->take(5)->get();



        return view('shop.listing', [
          'products' => $products
        ]);
        
        /* return view('shop.listing')->with('products', $products); */

        /* $data = $request->session()->all();
        dd($data);
        $data = $request->session()->get('_token'); */
        /* dd($data); */
        /* $data = 'simon'; */
        /* return view('greetings', ['name' => 'Victoria']); */
        /* return view('home')->with('data', $data); */
       /* dd("hello"); */

    }
}
