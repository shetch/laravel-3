<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class MetaNameServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // MetaTitleComposer
        View::composer(['*'], 'App\Core\Composers\MetaNameComposer');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // MetaTitleComposer
        View::composer(['*'], 'App\Core\Composers\MetaNameComposer');
    }
}