<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    /*Restrict forms to specific fields*/
    /*protected $fillable = ['title','description'];*/

    public function owner()
    {
        /*elequent relationships*/
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
         /*elequent relationships*/
         return $this->hasMany(Task::class);
    }

    public function addTask($task)
    {
        $this->tasks()->create($task);
    }
}
