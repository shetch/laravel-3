<?php namespace App\Core\Composers;

use Illuminate\View\View;
/* use Illuminate\Support\Arr; */

/**
 * Class MetaNameComposer
 *
 * @package App\Core\Composers
 */
class MetaNameComposer
{

    /**
     * Bind data to the view.
     *
    */

    public function compose(View $view)
    {

        $meta = array(
          "keywords" => "Add Keywords here",
          "description" => "Add Description here",
        );

        /* $meta = Arr::add(['keywords' => 'Add Keywords here'], 'description', 'Add Description here'); */
        
        $view->with('meta', $meta);
    }
}