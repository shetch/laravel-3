<?php namespace App\Core\Composers;

use Illuminate\View\View;

/**
 * Class PageTitleComposer
 *
 * @package App\Core\Composers
 */
class PageTitleComposer
{

    /**
     * Bind data to the view.
     *
    */

    public function compose(View $view)
    {
        $type = request()->input('type');
        $tech = request()->input('tech');

        $type = ucfirst(strtolower($type));
        $tech = ucfirst(strtolower($tech));

        if ($tech == 'Fdried') {
            $tech = 'Dried';
        }

        if ((strlen($type) == 0) && (strlen($tech) == 0)) {
            $metaTitle = 'My Site | Learning Laravel';
        }

        if (strlen($tech) > 0) {
            $metaTitle = $tech . ' ' . $type . ' Food | My Site | Learning Laravel';
        }

        if ((strlen($type) > 0) && (strlen($tech) == 0)) {
            $metaTitle = $type . ' Food | My Site | Learning Laravel';
        }
        $view->with('metaTitle', $metaTitle);
    }
}
