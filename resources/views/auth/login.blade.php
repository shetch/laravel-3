@extends('layouts.app')

@section('content')
 <div class="w-full max-w-md text-left mx-auto">
  <h1 class="text-3xl text-center uppercase mb-4">{{ __('Login') }}</h1>

    <form class="bg-white border border-solid border-gray-300 rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('login') }}">
    <!-- {{ csrf_field() }} -->
      @csrf
      <div class="mb-4">
       <label class="block text-blue-700 text-sm font-bold mb-2" for="email">{{ __('E-Mail Address') }}</label>
       <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('email') is-invalid @enderror"
        type="email"
        id="email"
        name="email"
        value="{{ old('email') }}" required autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <div class="mb-4">
        <label for="password" class="block text-blue-700 text-sm font-bold mb-2">{{ __('Password') }}</label>
        <input id="password" type="password" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <div class="mb-4">
        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        <label class="form-check-label" for="remember">
            {{ __('Remember Me') }}
        </label>
      </div>
      <div class="flex items-center justify-between">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
            {{ __('Login') }}
        </button>
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
      </div>

    </form>
 </div>


                           
@endsection
