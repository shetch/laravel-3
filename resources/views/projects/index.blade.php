@extends('layouts.app')

@section('content')


  <!--   <div class="w-full text-gray-900" style="min-height:600px;"> -->
        {{--<div class="p-2">
            <h1 class="text-center text-lg font-medium">
            Task Viewer
            </h1>
            <div class="alert bg-blue-300 text-sm test-white px-10 py-2" style="display:none">dfsdfdfsdfsdfsdfd</div>
        </div>--}}

  <div class="w-full" style="min-height:600px;">
        <table class="projects w-full bg-white mt-12">
            <tbody>
                <tr class="text-blue-500 border-b text-sm">
                    
                    <th class="text-left font-medium px-2">Task #</th>
                    <th class="text-center font-medium px-2"></th>
                    <th class="text-left font-medium px-2">Branch / Description</th>
                    <th class="text-center font-medium px-2">Start</th>
                    <th class="text-center font-medium px-2">Hold</th>
                    <th class="text-center font-medium px-2">WIP</th>
                    <th class="text-center font-medium px-2">Pull</th>
                    <th class="text-center font-medium px-2">Dev</th>
                    <th class="text-center font-medium px-2">Pull</th>
                    <th class="text-center font-medium px-2">Stage</th>
                    <th class="text-center font-medium px-2">Pull</th>
                    <th class="text-center font-medium px-2">Mast</th>
                    <th class="text-center font-medium px-2">Comp</th>
                </tr>

                @if (isset($projects))
                    @foreach ($projects as $project)

                        <?php
                        $td0 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td1 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td2 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td3 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td4 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td5 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td6 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td7 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td8 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';
                        $td9 = 'bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500';

                        switch ($project->status) {
                            case 0:
                                $td0 = 'bg-white border-4 border-red-500 hover:bg-red-300';
                                break;
                            case 1:
                                $td1 = 'bg-red-700 hover:bg-red-300';
                                break;
                            case 2:
                                $td2 = 'bg-white border-4 border-blue-500 hover:bg-blue-300';
                                break;
                            case 3:
                                $td3 = 'bg-blue-200 border-4 border-blue-500 hover:bg-blue-300';
                                break;
                            case 4:
                                $td4 = 'bg-blue-500';
                                break;
                            case 5:
                                $td5 = 'bg-orange-300 border-4 border-orange-500';
                                break;
                            case 6:
                                $td6 = 'bg-orange-500';
                                break;
                            case 7:
                                $td7 = 'bg-green-300 border-4 border-green-500';
                                break;
                            case 8:
                                $td8 = 'bg-green-500';
                                break;
                            case 9:
                                $td9 = 'bg-purple-600';
                                break;
                            default:
                                $td0 = 'bg-gray-400';
                        }
                        ?>

                <tr class="border-b hover:bg-orange-100 bg-gray-100">
                        
                    <td class="p-1 px-1 text-blue-500 text-sm font-medium">
                    <a href="{{ config('site.admin_url') }}/view-task.php?histid={{ $project->task_number }}" target="_blank">{{ $project->task_number }}</a></td>

                    <td class="h-12 flex flex-no-wrap items-center justify-center">
                        <a class="p-1 text-white bg-green-500 rounded" href="{{ config('site.admin_url') }}/view-customers.php?customerid={{ $project->customer }}" target="_blank"><i class="far fa-user"></i></a>
                        <a class="ml-1 p-1 text-white bg-blue-500 rounded" href="{{ config('site.admin_url') }}/view-customers.php?customerid={{ $project->customer }}#Domains" target="_blank"><i class="fas fa-globe"></i></a>
                        <a class="ml-1 p-1 text-white bg-orange-500 rounded" href="{{ config('site.admin_url') }}/view-customers.php?customerid={{ $project->customer }}#Logins" target="_blank"><i class="fas fa-lock"></i></a>
                    </td>
                    <td class="p-1 px-1 text-sm font-medium whitespace-no-wrap">
                        <h6 class="text-gray-800 text-sm font-medium">{{ $project->branch }}</h6>
                        <small class="text-blue-500 text-xs font-medium">{{ $project->note }}</small>
                    </td>

                    <td align="center" class="p-1 px-1"><a class="{{$td0}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=0" data-id="{{ $project->id }}" data-status="0"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td1}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=1" data-id="{{ $project->id }}" data-status="1"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td2}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=2" data-id="{{ $project->id }}" data-status="2"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td3}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=3" data-id="{{ $project->id }}" data-status="3"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td4}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=4" data-id="{{ $project->id }}" data-status="4"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td5}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=5" data-id="{{ $project->id }}" data-status="5"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td6}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=6" data-id="{{ $project->id }}" data-status="6"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td7}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=7" data-id="{{ $project->id }}" data-status="7"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td8}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=8" data-id="{{ $project->id }}" data-status="8"></a></td>
                    <td align="center" class="p-1 px-1"><a class="{{$td9}} text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats" href="/projects?id={{ $project->id }}&set=9" data-id="{{ $project->id }}" data-status="9"></a></td>

                            <!-- <td class="p-1 px-1"><input type="text" value="user.name" class="bg-transparent"></td>
                            <td class="p-3 px-1"><input type="text" value="user.email" class="bg-transparent"></td>
                            <td class="p-3 px-1">
                                <select value="user.role" class="bg-transparent">
                                    <option value="user">user</option>
                                    <option value="admin">admin</option>
                                </select>
                            </td>
                            <td class="p-3 px-1 flex justify-end"><button type="button" class="mr-3 text-sm bg-blue-500 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Save</button><button type="button" class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Delete</button></td> -->
                </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    
 </div>
    {{--<div class="">
        <div class="bg-orange-300" style="min-height:400px;">


        </div>
    </div>--}}



@endsection