@extends('layouts.app')

@section('content')
  <div class="w-full max-w-md text-left mx-auto">
  <h1 class="text-3xl text-center uppercase mb-4">Create Projects</h1>

    <form class="bg-white border border-solid border-gray-300 rounded px-8 pt-6 pb-8 mb-4" method="POST" action="/projects">
    <!-- {{ csrf_field() }} -->
      @csrf

      <div class="mb-4">
       <label class="block text-blue-700 text-sm font-bold mb-2" for="task_number">Task #</label>
       <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline {{ $errors->has('title') ? 'alert-danger' : '' }}"
             type="text"
             id="task_number"
             name="task_number"
             placeholder="Task Number"
             value="{{ old('task_number') }}" required>
      </div>
      <div class="mb-4">
        <label class="block text-blue-700 text-sm font-bold mb-2" for="note">Note</label>
        <textarea id="note" name="note" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline {{ $errors->has('note') ? 'alert-danger' : '' }}" placeholder="Project Note" required>{{ old('note') }}</textarea>
      </div>
      <div class="mb-4">
        <label class="block text-blue-700 text-sm font-bold mb-2" for="note">Branch</label>
        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             type="text"
             id="branch"
             name="branch"
             placeholder="Branch"
             value="">
      </div>
      <div class="mb-6">
        <label class="block text-blue-700 text-sm font-bold mb-2" for="status">
          Status
        </label>
        <div class="">
          <select 
            class="shadow appearance-none border rounded block w-full text-gray-700 py-3 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:shadow" 
            id="status"
            name="status">
              <option value="0">Not Started</option>
              <option value="1">Waiting</option>
              <option value="2">In Progress</option>
              <option value="3">Pull - Develop</option>
              <option value="4">Develop</option>
              <option value="5">Pull - Staging</option>
              <option value="6">Staging</option>
              <option value="7">Pull - Master</option>
              <option value="8">Master</option>
              <option value="9">Complete</option>
            </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>

      <div class="flex items-center justify-between">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
          Create Project
        </button>
      </div>

      @include('errors')

    </form>
  </div>
@endsection