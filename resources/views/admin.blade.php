@extends('layouts.admin')

@section('content')



<div class="md:flex md:w-3/5 mx-auto my-10">
  <div class="md:flex-shrink-0 md:w-2/5">
    <img class="rounded-lg" src="{{ asset('/img/task.svg') }}" alt="Woman paying for a purchase">
  </div>
  <div class="mt-4 md:mt-0 md:ml-6 md:w-3/5 flex flex-col justify-center">
    <div class="uppercase tracking-wide text-sm text-indigo-600 font-bold">Marketing</div>
    <a href="#" class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline">Admin for your new business</a>
    <p class="mt-2 text-gray-600">Getting a new business off the ground is a lot of hard work. Here are five ideas you can use to find your first customers.</p>
  </div>
</div>

<div class="md:flex md:w-3/5 mx-auto my-10">

<div class="mt-4 md:mt-0 md:ml-6 md:w-3/5 flex flex-col justify-center">
    <div class="uppercase tracking-wide text-sm text-indigo-600 font-bold">Marketing</div>
    <a href="#" class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline">Finding customers for your new business</a>
    <p class="mt-2 text-gray-600">Getting a new business off the ground is a lot of hard work. Here are five ideas you can use to find your first customers.</p>
  </div>
  <div class="md:flex-shrink-0 md:w-2/5">
    <img class="rounded-lg" src="{{ asset('/img/art.svg') }}" alt="Woman paying for a purchase">
  </div>

</div>

<div class="md:flex md:w-3/5 mx-auto my-10">
  <div class="md:flex-shrink-0 md:w-2/5">
    <img class="rounded-lg" src="{{ asset('/img/prof.svg') }}" alt="Woman paying for a purchase">
  </div>
  <div class="mt-4 md:mt-0 md:ml-6 md:w-3/5 flex flex-col justify-center">
    <div class="uppercase tracking-wide text-sm text-indigo-600 font-bold">Marketing</div>
    <a href="#" class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline">Finding customers for your new business</a>
    <p class="mt-2 text-gray-600">Getting a new business off the ground is a lot of hard work. Here are five ideas you can use to find your first customers.</p>
  </div>
</div>


@endsection