<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @if (isset($meta))
        @foreach($meta as $key => $value)
            <meta name="{{ $key }}" content="{{ $value }}"/>
        @endforeach
    @endif

    <link rel="canonical" href=""/>

  <title>
      @isset($metaTitle)
          {{ $metaTitle }}
      @endisset
  </title>

  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

  <title>@isset($metaTitle){{ $metaTitle }}@endisset</title>


</head>
<body>

<nav class="flex items-center justify-between flex-wrap bg-main p-6">
  <div class="flex items-center flex-shrink-0 text-white mr-6">
    <a href="/" class=""><img src="{{ asset('/img/logo.png') }}" class="h-10 max-h-full"></a>
  </div>
  <div class="block lg:hidden">
    <button class="flex items-center px-3 py-2 border rounded text-white border-teal-400 hover:text-white hover:border-white">
      <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div class="w-full block flex-grow lg:flex lg:items-start lg:w-auto">
    <div class="text-main font-medium flex w-full justify-end text-right">
      

      
     

        <!-- Authentication Links -->
        @guest
            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('login') }}">{{ __('Login') }}</a>

            @if (Route::has('register'))
                <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('register') }}">{{ __('Register') }}</a>
            @endif
        @else
            <a href="/admin" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
            Admin
            </a>


            <a href="/more" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
            More
            </a>
            <a href="/more-2" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
              More 2
            </a>

            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

        @endguest

    </div>
  </div>
</nav>


    <div class="w-full mx-auto mt-0">
      @yield('content')
    </div>

    <div class="flex flex-wrap overflow-hidden">

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

        <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/6 xl:w-1/6">
          <!-- Column Content -->
        </div>

    </div>


@include("layouts.footer")




</body>
</html>