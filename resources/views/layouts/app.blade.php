<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @if (isset($meta))
        @foreach($meta as $key => $value)
            <meta name="{{ $key }}" content="{{ $value }}"/>
        @endforeach
    @endif

    <link rel="canonical" href=""/>

  <title>
      @isset($metaTitle)
          {{ $metaTitle }}
      @endisset
  </title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css?ver=5.2.2">
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

  <title>@isset($metaTitle){{ $metaTitle }}@endisset</title>


</head>
<body>

<nav class="flex items-center justify-between flex-wrap bg-main p-6">
  <div class="flex items-center flex-shrink-0 text-white mr-6">
    <a href="/" class=""><img src="{{ asset('/img/logo.png') }}" class="h-10 max-h-full"></a>
  </div>
  <div class="block lg:hidden">
    <button class="flex items-center px-3 py-2 border rounded text-white border-teal-400 hover:text-white hover:border-white">
      <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div class="w-full block flex-grow lg:flex lg:items-start lg:w-auto">
    <div class="text-main font-medium flex w-full justify-end text-right">
     
      {{--<a href="/more" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
        More
      </a>
      <a href="/more-2" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
        More 2
      </a>--}}
      <a href="/listing" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
        Listing
        </a>
      <a href="/cart" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
        Cart
        </a>

        <!-- Authentication Links -->
        @guest
            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('login') }}">{{ __('Login') }}</a>

            @if (Route::has('register'))
                <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('register') }}">{{ __('Register') }}</a>
            @endif
        @else

        <a href="/projects" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
        View Projects
        </a>
        <a href="/projects/create" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
          Create Project
        </a>

        {{--<a href="/projects/2/edit" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
          Edit Project 2
        </a>
        <a href="/projects/2" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
          Show Project 2
        </a>
        <a href="/projects?page=1&sort=popularity&ingre=&type=DOG&tech=WET" class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4">
          Query
        </a>--}}


            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }} <span class="caret"></span>
          </a>

            <a class="block mt-4 lg:inline-block lg:mt-0 text-gray-400 hover:text-gray-400 mr-4" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

        @endguest

    </div>
  </div>
</nav>

<div class="w-12/12 mx-auto mt-0" style="max-width:1500px;">
  @yield('content')
</div>

@include("layouts.footer")

<script src="http://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous">
</script>
<script>
         $(document).ready(function(){
            $('.projects tr td .stats').click(function(e){
              e.preventDefault();

              var item = $(this);
              var id = $(this).attr("data-id");
              var taskStatus = parseInt($(this).attr("data-status"));

              var msg;

              switch (taskStatus) {
                case 0:
                    msg = 'bg-white border-4 border-red-500 hover:bg-red-300';
                    break;
                case 1:
                    msg = 'bg-red-700 hover:bg-red-300';
                    break;
                case 2:
                      msg = 'bg-white border-4 border-blue-500 hover:bg-blue-300';
                    break;
                case 3:
                    msg = 'bg-blue-200 border-4 border-blue-500 hover:bg-blue-300';
                    break;
                case 4:
                      msg = 'bg-blue-500';
                    break;    
                case 5:
                    msg = 'bg-orange-300 border-4 border-orange-500';
                    break;
                case 6:
                    msg = 'bg-orange-500';
                    break;
                case 7:
                      msg = 'bg-green-300 border-4 border-green-500';
                    break;
                case 8:
                    msg = 'bg-green-500';
                    break;
                case 9:
                      msg = 'bg-purple-600';
                    break; 
                default:
                    msg = 'bg-gray-400';
              }

              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
                url: "{{ url('/projects/post') }}",
                method: 'post',
                data: {
                    id: id,
                    status: taskStatus,
                    msg: msg
                },
                success: function(result){
                    item.parent().parent().find(".stats").attr("class","bg-white border-4 border-gray-300 hover:border-4 hover:border-gray-500 text-white block w-6 h-6 font-bold rounded-full focus:outline-none focus:shadow-outline stats");
                    item.attr("class",msg+" text-white block w-6 h-6 font-bold rounded-full active:outline-none stats");
                },
                error: function(e) {
                    $('.alert').show();
                    $('.alert').html('Error' + e);
                }
              });
            });


            $('.ajaxSubmit').click(function(e){
               e.preventDefault();
               var id = $(this).attr("data-id");
               var taskStatus = 1;
               $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               $.ajax({
                  url: "{{ url('/projects/post') }}",
                  method: 'post',
                  data: {
                     id: id,
                     status: taskStatus
                  },
                  success: function(result){
                     $('.alert').show();
                     $('.alert').html(result.success);
                  },
                  error: function(e) {
                      $('.alert').show();
                      $('.alert').html('Error' + e);
                  }
                });
               });



               /*Swal.fire({
                title: 'Error!',
                text: 'Do you want to continue',
                type: 'error',
                confirmButtonText: 'Cool'
               });*/


            });
</script>
@include('sweetalert::alert')

</body>
</html>