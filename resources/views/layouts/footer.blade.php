<div class="w-full text-white bg-gray-800">
<div class="w-full overflow-hidden sm:w-full md:w-5/6 lg:w-5/6 xl:w-5/6 mx-auto py-12" style="max-width:1500px;">
    <div class="sm:flex mb-4 text-sm">
  <div class="sm:w-1/4 h-auto">
      <div class="text-white mb-2">white</div>
      <ul class="list-reset leading-normal">
          <li class="hover:text-white text-grey-darker">One</li>
          <li class="hover:text-white text-grey-darker">Two</li>
          <li class="hover:text-white text-grey-darker">Three</li>
          <li class="hover:text-white text-grey-darker">Four</li>
          <li class="hover:text-white text-grey-darker">Five</li>
          <li class="hover:text-white text-grey-darker">Six</li>
          <li class="hover:text-white text-grey-darker">Seven</li>
          <li class="hover:text-white text-grey-darker">Eight</li>
      </ul>
  </div>
  <div class="sm:w-1/4 h-auto sm:mt-0 mt-8">
      <div class="text-blue mb-2">Blue</div>
      <ul class="list-reset leading-normal">
          <li class="hover:text-blue text-grey-darker">One</li>
          <li class="hover:text-blue text-grey-darker">Two</li>
          <li class="hover:text-blue text-grey-darker">Three</li>
      </ul>

    <div class="text-blue-light mb-2 mt-4">Blue-light</div>
      <ul class="list-reset leading-normal">
          <li class="hover:text-blue-light text-grey-darker">One</li>
          <li class="hover:text-blue-light text-grey-darker">Two</li>
          <li class="hover:text-blue-light text-grey-darker">Three</li>
      </ul>

  </div>
  <div class="sm:w-1/4 h-auto sm:mt-0 mt-8">
     <div class="text-green-dark mb-2">Green-dark</div>
      <ul class="list-reset leading-normal">
          <li class="hover:text-green-dark text-grey-darker">One</li>
          <li class="hover:text-green-dark text-grey-darker">Two</li>
          <li class="hover:text-green-dark text-grey-darker">Three</li>
      </ul>

    <div class="text-green-light mb-2 mt-4">Green-light</div>
      <ul class="list-reset leading-normal">
          <li class="hover:text-green-light text-grey-darker">One</li>
          <li class="hover:text-green-light text-grey-darker">Two</li>
          <li class="hover:text-green-light text-grey-darker">Three</li>
      </ul>


  </div>
    <div class="sm:w-1/2 sm:mt-0 mt-8 h-auto">
        <div class="text-red-light mb-2">Newsletter</div>
        <p class="text-grey-darker leading-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, consectetur. </p>
        <div class="mt-4 flex">
            <input type="text" class="p-2 border border-grey-light round text-grey-dark text-sm h-auto" placeholder="Your email address">
            <button class="bg-orange-600 text-white rounded-sm h-auto text-xs p-3">Subscribe</button>
        </div>
    </div>

</div>
</div>