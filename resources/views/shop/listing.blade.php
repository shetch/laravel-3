@extends('layouts.app')

@section('content')

<div class="w-full md:flex" style="min-height:600px;">
  @if (isset($products))
      @foreach ($products as $product)
       <div class="w-1/4 p-6 rounded-lg bg-white text-center">
        <div class="w-100">
          <img 
            src="{{ asset('/img/products') }}/{{ $product->img }}" 
            alt="{{ $product->name }}" 
            class="w-100">
        </div>
        <div class="w-100 text-lg text-blue-500 mb-2">£{{ $product->price }}</div>
        <a class="bg-blue-500 hover:bg-blue-300 text-white rounded px-2 py-1" href="/cart?id={{ $product->id }}" alt="Buy Now"><i class="fas fa-shopping-cart text-white"></i> Buy Now</a>
       </div>
      @endforeach
  @endif
</div>

@endsection