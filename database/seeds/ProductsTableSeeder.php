<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('products')->insert([
            'name' => Str::random(48),
            'description' => Str::random(100),
            'price' => 2.99,
            'img' => '1.jpg',
            'category_id' => 1,
        ]);*/

        factory(App\Product::class, 50)->create()->each(function ($product) {
            /*$product->posts()->save(factory(App\Product::class)->make());*/
        });


    }
}
