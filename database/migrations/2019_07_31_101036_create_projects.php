<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('owner_id')->index();
            $table->string('task_number');
            $table->string('branch')->nullable();
            $table->text('note');
            $table->unsignedInteger('status');
            $table->unsignedInteger('mode');
            $table->unsignedInteger('customer');
            $table->text('notes');
            $table->timestamps();
           /* $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
