<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Route::get('/', 'ProjectsController@index'); */

Route::get('/more', function () {
  return view('/more')->with('metaTitle', 'My Site | Learning Laravel');
})->name('more');
Route::get('/more-2', function () {
  return view('/more-2')->with('metaTitle', 'My Site | Learning Laravel');
});

/* Route::get('/', function () {
    return view('welcome')->with('metaTitle', 'My Site | Learning Laravel');
}); */

/* 
  GET /projects (index)
  GET /projects/create (create)
  GET /projects/1 (show)

  POST /projects (store)
  GET /projects/1/edit (edit)

  PATCH  /projects/1 (update)
  DELETE /projects/1 (delete)

*/


Route::resource('projects', 'ProjectsController');
Route::post('/projects/post', 'ProjectsController@post');


/* Route::get('/projects', 'ProjectsController@index');
Route::get('/projects/create', 'ProjectsController@create');
Route::get('/projects/{id}/show', 'ProjectsController@show');
Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/{id}/edit', 'ProjectsController@edit');
Route::patch('/projects/{id}', 'ProjectsController@update');
Route::delete('/projects/{id}', 'ProjectsController@destroy'); */
Auth::routes();

/*Route::get('/home', 'HomeController@index')->middleware('auth');*/
/* Route::get('/home', 'HomeController@index')->name('home'); */

/* Route::get('/admin', 'AdminController@index')->name('admin'); */

Route::get('/', 'HomeController@index');
Route::get('/listing', 'ListingController@index');

Route::get('/cart', 'CartController@index');

Route::group(['middleware' => ['web','auth']], function(){
 /*  Route::get('/', function () {
    if (Auth::user()->admin == 0) {
      return view('home');
    } else {
      $users['users'] = \App\User::all();
      return view('admin', $users);
    }
  }); */  
  Route::get('/admin', function () {
    if (Auth::user()->admin == 0) {
      return view('home');
    } else {
      $users['users'] = \App\User::all();
      return view('admin', $users);
    }
  });

});

